
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <!-- img src='img/hex-logo.png' align="right" height="200" /--> md4e 📦

## Markdown for Education

<!-- badges: start -->

[![Pipeline
status](https://gitlab.com/r-packages/md4e/badges/prod/pipeline.svg)](https://gitlab.com/r-packages/md4e/commits/prod)

[![Coverage
status](https://codecov.io/gl/r-packages/md4e/branch/prod/graph/badge.svg)](https://codecov.io/gl/r-packages/md4e?branch=prod)

<!-- [![Dependency status](https://tinyverse.netlify.com/badge/md4e)](https://CRAN.R-project.org/package=md4e) -->

<!-- badges: end -->

The pkgdown website for this project is located at
<https://r-packages.gitlab.io/md4e>.

<!--------------------------------------------->

<!-- Start of a custom bit for every package -->

<!--------------------------------------------->

Its simple separation of content and styling makes Markdown well-suited
for development of course material. Including R chunks allows directly
producing plots and tables, and the support for math expressions enables
including equations. Markdown for Education preprocesses (R) Markdown
files and exports them to HTML to be imported into a Learning Management
System, also offering functionality for itembank production.

<!--------------------------------------------->

<!--  End of a custom bit for every package  -->

<!--------------------------------------------->

## Installation

You can install the released version of `md4e` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('md4e');
```

You can install the development version of `md4e` from
[GitLab](https://gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/md4e');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

<!--------------------------------------------->

<!-- Start of a custom bit for every package -->

<!--------------------------------------------->

<!--------------------------------------------->

<!--  End of a custom bit for every package  -->

<!--------------------------------------------->
